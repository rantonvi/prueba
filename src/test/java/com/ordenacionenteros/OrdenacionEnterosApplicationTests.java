package com.ordenacionenteros;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.ordenacionenteros.rest.OrdenarNumeros;

@RunWith(SpringRunner.class)
@WebMvcTest(value = OrdenarNumeros.class)
class OrdenacionEnterosApplicationTests{
	private static Logger log  = LoggerFactory.getLogger(OrdenacionEnterosApplicationTests.class);
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void contextLoads() throws Exception{
		
		String expectedJson = "";
		
		String URI = "/sortbinarios/5/6/500/10/1";
			RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).content(expectedJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
  
		String output = result.getResponse().getContentAsString();
		String [] arrayOutput = output.split(",");
				
		for(int i=0;i<arrayOutput.length;i++) {
			log.info("Datos de Salida" + arrayOutput[i]);			
		}
			
		assertEquals(200,result.getResponse().getStatus());
	}	
}
