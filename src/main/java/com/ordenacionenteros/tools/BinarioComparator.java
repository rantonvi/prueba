package com.ordenacionenteros.tools;

import java.util.Comparator;
import com.ordenacionenteros.model.Binarios;

public class BinarioComparator implements Comparator<Binarios> {
	@Override
	public int compare(Binarios o1, Binarios o2) {
		if(o1.getNumeroDeUnos().equals(o2.getNumeroDeUnos())) {
			return new Integer(o2.getNumero()).compareTo(new Integer(o1.getNumero()));
		}else {
			return new Integer(o1.getNumeroDeUnos()).compareTo(new Integer(o2.getNumeroDeUnos()));
		}
	}
}
