package com.ordenacionenteros.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ordenacionenteros.model.Binarios;
import com.ordenacionenteros.tools.BinarioComparator;

@RestController
@RequestMapping(OrdenarNumeros.SORTBINARIOS)
public class OrdenarNumeros {
	public static final String SORTBINARIOS = "/sortbinarios";
	List<Binarios> listEntrada = new ArrayList<>();
	
	@GetMapping(value = "/{numero1}/{numero2}/{numero3}/{numero4}/{numero5}")
	public List<Binarios> listar(@PathVariable("numero1") Integer numero1,@PathVariable("numero2") Integer numero2,@PathVariable("numero3") Integer numero3,
			@PathVariable("numero4") Integer numero4, @PathVariable("numero5") Integer numero5){		
		
		listEntrada.clear();
		
		insertarBinarios(numero1);
		insertarBinarios(numero2);
		insertarBinarios(numero3);
		insertarBinarios(numero4);
		insertarBinarios(numero5);
						
		Collections.sort(listEntrada, new BinarioComparator().reversed());	
		
		return listEntrada;				
	}
	
	public void insertarBinarios(int numero) {
		Binarios binario = new Binarios();	
		binario.setNumero(numero);		
		binario.setBinario(transformarDeEnteroABinario(numero));
		binario.setNumeroDeUnos(obtenerNumeroDeUnos(transformarDeEnteroABinario(numero)));
		listEntrada.add(binario);
	}
	
	public String obtenerNumeroDeUnos(String binario) {
		String result  = binario.replace("0","");
		String r = ""+result.trim().length();
		return r;		
	}
	
	public String transformarDeEnteroABinario(int numero) {		
		String binario = "";
		if (numero > 0) {
			while (numero > 0) {
				if (numero % 2 == 0) {
					binario = "0" + binario;
				} else {
					binario = "1" + binario;
				}
				numero = (int) numero / 2;
			}
		}
		return binario;
	}
}
