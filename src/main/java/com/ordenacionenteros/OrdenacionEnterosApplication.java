package com.ordenacionenteros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdenacionEnterosApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdenacionEnterosApplication.class, args);
	}

}
