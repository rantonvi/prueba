package com.ordenacionenteros.model;

public class Binarios {
	int numero;
	String binario;
	String numeroDeUnos;
	public String getNumeroDeUnos() {
		return numeroDeUnos;
	}
	public void setNumeroDeUnos(String numeroDeUnos) {
		this.numeroDeUnos = numeroDeUnos;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getBinario() {
		return binario;
	}
	public void setBinario(String binario) {
		this.binario = binario;
	}	
}
